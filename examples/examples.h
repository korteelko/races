#ifndef EXAMPLES_EXAMPLES_H_
#define EXAMPLES_EXAMPLES_H_

#include "tracking.h"


void print_angles(const Tracking::track_points &track);

void run_example1();
void run_example2();

#endif  // !EXAMPLES_EXAMPLES_H_
