#include "examples.h"
#include "tracking.h"

#include <iostream>

#include <assert.h>


using namespace track_geom;

static Tracking::track_points test_track {
  pos(6.0, 3.0),
  pos(9.0, 5.0),
  pos(10.0, 7.0),
  pos(10.0, 9.0),
  pos(10.0, 12.0),
  pos(9.0, 13.0),
  pos(7.0, 10.0),
  pos(14.0, 12.0),
  pos(11.0, 10.0),
  pos(10.5, 9.5),
  pos(10.5, 2.0),
  pos(3.0, 2.0),
  pos(4.0, 3.0),
  pos(3.0, 4.0),
  pos(4.0, 5.0),
  pos(4.0, 6.0),
};

static Tracking::metric_points standard_metric {
  // 1
  vector_auto {pos(6.3, 3.31), pos_polar(1.0, 0.588)},
  vector_auto {pos(8.0, 4.2), pos_polar(1.0, 0.588)},
  // 2
  vector_auto {pos(9.5, 6.0), pos_polar(1.0, 1.107)},
  // 3
  vector_auto {pos(10., 8.), pos_polar(1.0, 1.5708)},
  // 4
  vector_auto {pos(10., 10.), pos_polar(1.0, 1.5708)},
  vector_auto {pos(10., 11.2), pos_polar(1.0, 1.5708)},
  // 5
  vector_auto {pos(9.3, 12.8), pos_polar(1.0, 2.356)},
  // 6
  vector_auto {pos(8.7, 12.6), pos_polar(1.0, -2.159)},
  vector_auto {pos(8., 11.5), pos_polar(1.0, -2.159)},
  // 7
  vector_auto {pos(8.2, 10.1), pos_polar(1.0, 0.2783)},
  vector_auto {pos(10.6, 10.9), pos_polar(1.0, 0.2783)},
  vector_auto {pos(13.1, 11.7), pos_polar(1.0, 0.2783)},
  // 8
  vector_auto {pos(13.1, 11.7), pos_polar(1.0, -2.5536)},
  vector_auto {pos(12.3, 10.8), pos_polar(1.0, -2.5536)},
  // 9
  vector_auto {pos(10.7, 9.8), pos_polar(1.0, -2.3562)},
  // 10
  vector_auto {pos(10.1, 8.7), pos_polar(1.0, -1.5708)},
  vector_auto {pos(10.2, 7.8), pos_polar(1.0, -1.5708)},
  vector_auto {pos(10., 5.3), pos_polar(1.0, -1.5708)},
  // 11
  vector_auto {pos(9.7, 2.), pos_polar(1.0, 3.1416)},
  vector_auto {pos(7., 2.), pos_polar(1.0, 3.1416)},
  vector_auto {pos(4.8, 2.1), pos_polar(1.0, 3.1416)},
  // 12
  vector_auto {pos(3.2, 2.4), pos_polar(1.0, 0.7853)},
  // 13
  vector_auto {pos(3.4, 3.2), pos_polar(1.0, 2.35619)},
  // 14
  vector_auto {pos(3.3, 4.6), pos_polar(1.0, 0.7854)},
  // 15
  vector_auto {pos(4.0, 5.5), pos_polar(1.0, 1.5708)}
};

static Tracking::metric_points pitline_metric {
  // 5
  vector_auto {pos(9.3, 12.8), pos_polar(1.0, 2.356)},
  // 6
  vector_auto {pos(8.7, 12.6), pos_polar(1.0, -2.159)},
  vector_auto {pos(8., 11.5), pos_polar(1.0, -2.159)},
  // 7
  vector_auto {pos(8.2, 10.1), pos_polar(1.0, 0.2783)},
  vector_auto {pos(10.6, 10.9), pos_polar(1.0, 0.2783)},
  vector_auto {pos(13.1, 11.7), pos_polar(1.0, 0.2783)},
  // 8
  vector_auto {pos(13.1, 11.7), pos_polar(1.0, -2.5536)},
  vector_auto {pos(12.3, 10.8), pos_polar(1.0, -2.5536)},
  // 9
  vector_auto {pos(10.7, 9.8), pos_polar(1.0, -2.3562)},
  // 10
  vector_auto {pos(10.1, 8.7), pos_polar(1.0, -1.5708)},
  vector_auto {pos(10.2, 7.8), pos_polar(1.0, -1.5708)},
  vector_auto {pos(10., 5.3), pos_polar(1.0, -1.5708)},
  // 11
  vector_auto {pos(9.7, 2.), pos_polar(1.0, 3.1416)},
  vector_auto {pos(7., 2.), pos_polar(1.0, 3.1416)},
  vector_auto {pos(4.8, 2.1), pos_polar(1.0, 3.1416)},
  // 12
  vector_auto {pos(3.2, 2.4), pos_polar(1.0, 0.7853)},
  // 13
  vector_auto {pos(3.4, 3.2), pos_polar(1.0, 2.35619)},
  // 14
  vector_auto {pos(3.3, 4.6), pos_polar(1.0, 0.7854)},
  // 15
  vector_auto {pos(4.0, 5.5), pos_polar(1.0, 1.5708)}
};

static Tracking::metric_points pitstop_metric {
  // 1
  vector_auto {pos(6.3, 3.31), pos_polar(1.0, 0.588)},
  vector_auto {pos(8.0, 4.2), pos_polar(1.0, 0.588)},
  // 2
  vector_auto {pos(9.5, 6.0), pos_polar(1.0, 1.107)},
  // 3
  vector_auto {pos(10., 8.), pos_polar(1.0, 1.5708)},
  // 4
  vector_auto {pos(10., 10.), pos_polar(1.0, 1.5708)},
  vector_auto {pos(10., 11.2), pos_polar(0.0, 1.5708)},
  // 5
  vector_auto {pos(9.3, 12.8), pos_polar(1.0, 2.356)},
  // 6
  vector_auto {pos(8.7, 12.6), pos_polar(1.0, -2.159)},
  vector_auto {pos(8., 11.5), pos_polar(1.0, -2.159)},
  // 7
  vector_auto {pos(8.2, 10.1), pos_polar(1.0, 0.2783)},
  vector_auto {pos(10.6, 10.9), pos_polar(1.0, 0.2783)},
  vector_auto {pos(13.1, 11.7), pos_polar(1.0, 0.2783)},
  // 8
  vector_auto {pos(13.1, 11.7), pos_polar(1.0, -2.5536)},
  vector_auto {pos(12.3, 10.8), pos_polar(1.0, -2.5536)},
  // 9
  vector_auto {pos(10.7, 9.8), pos_polar(1.0, -2.3562)},
  // 10
  vector_auto {pos(10.1, 8.7), pos_polar(0.0, -1.5708)},
  vector_auto {pos(10.2, 7.8), pos_polar(1.0, -1.5708)},
  vector_auto {pos(10., 5.3), pos_polar(1.0, -1.5708)},
  // 11
  vector_auto {pos(9.7, 2.), pos_polar(1.0, 3.1416)},
  vector_auto {pos(7., 2.), pos_polar(0.0, 3.1416)},
  vector_auto {pos(4.8, 2.1), pos_polar(1.0, 3.1416)},
  // 12
  vector_auto {pos(3.2, 2.4), pos_polar(1.0, 0.7853)},
  // 13
  vector_auto {pos(3.4, 3.2), pos_polar(1.0, 2.35619)},
  // 14
  vector_auto {pos(3.3, 4.6), pos_polar(1.0, 0.7854)},
  // 15
  vector_auto {pos(4.0, 5.5), pos_polar(1.0, 1.5708)}
};

static Tracking::metric_points crash_metric {
  // 1
  vector_auto {pos(6.3, 3.31), pos_polar(1.0, 0.588)},
  vector_auto {pos(8.0, 4.2), pos_polar(1.0, 0.588)},
  // 2
  vector_auto {pos(9.5, 6.0), pos_polar(1.0, 1.107)},
  // 3
  vector_auto {pos(10., 8.), pos_polar(1.0, 1.5708)},
  // 4
  vector_auto {pos(10., 10.), pos_polar(1.0, 1.5708)},
  vector_auto {pos(10., 11.2), pos_polar(1.0, 1.5708)},
  // 5
  vector_auto {pos(9.3, 12.8), pos_polar(1.0, 2.356)},
  // 6
  vector_auto {pos(8.7, 12.6), pos_polar(1.0, -2.159)},
  vector_auto {pos(8., 11.5), pos_polar(1.0, -2.159)},
  // 7
  vector_auto {pos(8.2, 10.1), pos_polar(1.0, 0.2783)},
  vector_auto {pos(10.6, 10.9), pos_polar(1.0, 0.2783)},
  vector_auto {pos(13.1, 11.7), pos_polar(1.0, 0.2783)},
  // 8
  vector_auto {pos(13.1, 11.7), pos_polar(1.0, -2.5536)},
  vector_auto {pos(12.3, 10.8), pos_polar(1.0, -2.5536)},
  // 9
  vector_auto {pos(10.7, 9.8), pos_polar(1.0, -2.3562)},
  // 10
  vector_auto {pos(10.1, 8.7), pos_polar(1.0, -1.5708)},
  vector_auto {pos(10.2, 7.8), pos_polar(1.0, -1.5708)},
  vector_auto {pos(10., 5.3), pos_polar(1.0, -1.5708)},
  // 11
  vector_auto {pos(9.7, 2.), pos_polar(1.0, 3.1416)},
  vector_auto {pos(7., 2.), pos_polar(0.0, 3.1416)},
  vector_auto {pos(4.8, 2.1), pos_polar(0.0, 3.1416)},
};

void print_angles(const Tracking::track_points &track) {
  const track_geom::pos *b = &track[0];
  for (auto x = track.begin() + 1; x != track.end(); ++x) {
    std::cout << "Angle of #" << std::distance(x, track.begin()) << " is "
        << track_geom::angle3(track_geom::pos(b->x + 1.0, b->y), *b, *x) << std::endl;
    b = x.base();
  }
}

// metric standard
// metric pitline
// metric pitstop
// metric crash

void run_example1() {
  print_angles(test_track);
  std::cout << std::endl;
  auto m_vec = {&standard_metric, &pitline_metric,
      &pitstop_metric, &crash_metric};
  for (auto m: m_vec) {
    Tracking tr(test_track, *m);
    auto r = tr.GetProjection();
    assert(r.size() == m->size());
    for (size_t i = 0; i < r.size(); ++i)
      std::cout << m->operator[](i).p << " --> " << r[i].projected_pos
          << " sec num = " << r[i].track_section_id << std::endl;
    std::cout << std::endl << std::endl;
  }
}
