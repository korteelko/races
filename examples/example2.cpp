#include "examples.h"
#include "tracking.h"

#include <iostream>

#include <assert.h>


static float pi = 3.14159266;

using namespace track_geom;

static Tracking::track_points kov_track {
  pos(2.0, 2.0),    // 0
  pos(2.0, 8.0),    // 1
  pos(3.0, 8.0),    // 2
  pos(3.0, 2.0),    // 3
  pos(4.0, 2.0),    // 4
  pos(4.0, 8.0),    // 5
  pos(5.0, 8.0),    // 6
  pos(5.0, 2.0),    // 7
  pos(6.0, 2.0),    // 8
  pos(6.0, 5.0),    // 9
  pos(1.0, 5.0),    // 10
  pos(1.0, 2.01),   // 11
  pos(2.01, 2.01),  // 12
  pos(2.01, 8.01),  // 13
  pos(3.01, 8.01),  // 14
  pos(3.01, 2.01),  // 15
  pos(4.01, 2.01),  // 16
  pos(4.01, 8.01),  // 17
  pos(5.01, 8.01),  // 18
  pos(5.01, 2.01),  // 19
  pos(8.01, 2.01),  // 20
};

static Tracking::metric_points standard_metric {
#ifndef NODEFINED
  /* другая точка ближе */
  vector_auto {pos(2.02, 2.02), pos_polar(1.0, 0.5 * pi)},
#endif  // NODEFINED
#ifdef NODEFINED1
  /* точка не проецируется на отрезок */
  vector_auto {pos(1.98, 1.98), pos_polar(0.0, 0.5 * pi)},
#endif  // NODEFINED1
  // 1
  vector_auto {pos(2.0, 2.01), pos_polar(0.0, 0.5 * pi)},
  vector_auto {pos(2.0, 6.02), pos_polar(1.0, 0.5 * pi)},
  // 2
  vector_auto {pos(2.2, 8.02), pos_polar(1.0, 0.0)},
  // 3
  vector_auto {pos(2.9, 7.98), pos_polar(1.0, 1.5 * pi)},
  vector_auto {pos(3.05, 2.98), pos_polar(1.0, 1.5 * pi)},
  // 4
  vector_auto {pos(3.8, 2.05), pos_polar(1.0, 0.0)},
  // 5
  vector_auto {pos(4.01, 3.02), pos_polar(1.0, 0.5 * pi)},
  // 6
  vector_auto {pos(4.73, 7.92), pos_polar(1.0, 0.0)},
  // 7
  vector_auto {pos(4.9, 7.91), pos_polar(1.0, 1.5 * pi)},
  vector_auto {pos(5.05, 3.03), pos_polar(1.0, 1.5 * pi)},
  // 8
  vector_auto {pos(5.8, 2.01), pos_polar(0.0, 0.0)},
  vector_auto {pos(5.8, 2.01), pos_polar(0.0, 0.0)},
  vector_auto {pos(5.8, 2.01), pos_polar(0.1, 0.0)},
  // 9
  vector_auto {pos(6.01, 4.02), pos_polar(1.0, 0.5 * pi)},
  // 10
  vector_auto {pos(4.73, 5.02), pos_polar(1.0, pi)},
  vector_auto {pos(2.11, 5.0), pos_polar(1.0, pi)},
  // 11
  vector_auto {pos(0.95, 5.91), pos_polar(1.0, 1.5 * pi)},
  // 12
  vector_auto {pos(1.8, 2.01), pos_polar(1.0, 0.0)},
  // 13
  vector_auto {pos(2.01, 3.02), pos_polar(1.0, 0.5 * pi)},
  vector_auto {pos(1.94, 4.96), pos_polar(1.0, 0.5 * pi)},
  // 14
  vector_auto {pos(2.73, 8.02), pos_polar(1.0, 0.0)},
  // а тут пошли точки такие же 3 - 7
  // 15
  vector_auto {pos(2.9, 7.98), pos_polar(1.0, 1.5 * pi)},
  vector_auto {pos(3.05, 2.98), pos_polar(1.0, 1.5 * pi)},
  // 16
  vector_auto {pos(3.8, 2.05), pos_polar(1.0, 0.0)},
  // 17
  vector_auto {pos(4.01, 3.02), pos_polar(1.0, 0.5 * pi)},
  // 18
  vector_auto {pos(4.73, 7.92), pos_polar(1.0, 0.0)},
  // 19
  vector_auto {pos(4.9, 7.91), pos_polar(1.0, 1.5 * pi)},
  vector_auto {pos(5.05, 3.03), pos_polar(1.0, 1.5 * pi)},
};

static Tracking::metric_points ts_metric {
  // 2
  vector_auto {pos(2.2, 8.02), pos_polar(1.0, 0.0)},
  // 3
  vector_auto {pos(2.9, 7.98), pos_polar(1.0, 1.5 * pi)},
  vector_auto {pos(3.05, 2.98), pos_polar(1.0, 1.5 * pi)},
  // 4
  vector_auto {pos(3.8, 2.05), pos_polar(1.0, 0.0)},
  // 5
  vector_auto {pos(4.01, 3.02), pos_polar(1.0, 0.5 * pi)},
  vector_auto {pos(4.01, 4.3), pos_polar(1.0, 0.5 * pi)},
  vector_auto {pos(4.01, 3.3), pos_polar(1.0, 0.5 * pi)},
  // 6
  vector_auto {pos(4.73, 7.99), pos_polar(1.0, 0.0)},
  // 7
  vector_auto {pos(4.99, 7.99), pos_polar(1.0, 1.5 * pi)},
  // снова 6
  vector_auto {pos(4.99, 7.92), pos_polar(1.0, 0.0)},
  // 7
  vector_auto {pos(5.05, 3.03), pos_polar(1.0, 1.5 * pi)},
  // 8
  vector_auto {pos(5.8, 2.01), pos_polar(0.0, 0.0)},
  vector_auto {pos(5.8, 2.01), pos_polar(0.0, 0.0)},
  vector_auto {pos(5.8, 2.01), pos_polar(0.1, 0.0)},
};

static Tracking::metric_points e_metric {
  vector_auto {pos(4.01, 6.01), pos_polar(0.1, 0.5 * pi)},
  vector_auto {pos(4.01, 6.01), pos_polar(2.1, 0.5 * pi)},
  vector_auto {pos(4.01, 8.01), pos_polar(2.1, 0.5 * pi)},
};

/* вообще в задании явно сказано о разнонаправленности близких участков,
 *   но что нам мешает
 * UPD: они могут быть сонаправлены */
void run_example2() {
  print_angles(kov_track);
  std::cout << std::endl;
  auto m_vec = {&standard_metric, &ts_metric, &e_metric};
  for (auto m: m_vec) {
    Tracking tr(kov_track, *m);
    auto r = tr.GetProjection();
    assert(r.size() == m->size());
    for (size_t i = 0; i < r.size(); ++i)
      std::cout << m->operator[](i).p << " --> " << r[i].projected_pos
          << " sec num = " << r[i].track_section_id << std::endl;
    std::cout << std::endl << std::endl;
  }
}
