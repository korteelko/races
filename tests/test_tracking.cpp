#include "tracking.h"

#include "gtest/gtest.h"

#include <cmath>
#include <memory>


static float pi = 3.1415926535;

using namespace track_geom;

/* улитошка */
static Tracking::track_points ulit = {
  pos(2.0, -3.0), pos(1.0, -3.0), pos(1.0, -2.0), pos(3.0, -2.0),
  pos(3.0, -4.0), pos(0.0, -4.0), pos(0.0, -1.0), pos(4.0, -1.0),
  pos(4.0, -5.0), pos(-1.0, -5.0), pos(-1.0, 0.0), pos(5.0, 0.0),
  pos(5.0, -6.0), pos(-2.0, -6.0), pos(-2.0, 1.0), pos(6.0, 1.0),
  pos(6.0, -2.0)
};
static Tracking::metric_points mp {
  // 1
  // стоим
  vector_auto{pos(1.2, -3.0), pos_polar(0.0, 0.0)},
  vector_auto{pos(1.2, -3.0), pos_polar(0.0, 0.0)},
  vector_auto{pos(1.2, -3.0), pos_polar(0.0, 0.0)},
  vector_auto{pos(1.2, -3.0), pos_polar(0.0, 0.0)},
  vector_auto{pos(1.2, -3.0), pos_polar(0.0, 0.0)},
  // едем
  vector_auto{pos(1.14, -3.2), pos_polar(0.2, pi)},
  vector_auto{pos(1.1, -2.8), pos_polar(0.2, pi)},
  // 2
  vector_auto{pos(1.1, -2.8), pos_polar(0.2, pi/2)},
  vector_auto{pos(1.1, -2.2), pos_polar(0.4, pi/2)},
  // 3
  vector_auto{pos(1.3, -2.0), pos_polar(0.4, 0.0)},
  vector_auto{pos(2.3, -2.0), pos_polar(0.4, 0.0)},
  // 4
  vector_auto{pos(3.1, -3.0), pos_polar(0.4, -pi/2)},
  // 5
  vector_auto{pos(1.5, -4.0), pos_polar(0.4, pi)},
  // 6
  vector_auto{pos(0.1, -2.8), pos_polar(0.2, pi/2)},
  vector_auto{pos(-0.09, -2.2), pos_polar(0.4, pi/2)},
  // 7
  vector_auto{pos(1.3, -1.1), pos_polar(0.4, 0.0)},
  vector_auto{pos(2.3, -1.0), pos_polar(0.4, 0.0)},
  // 8
  // стоим
  vector_auto{pos(4.0, -2.8), pos_polar(0.0, 0.0)},
  vector_auto{pos(4.0, -2.8), pos_polar(0.0, 1.5 * pi)},
  vector_auto{pos(4.0, -2.8), pos_polar(0.0, -pi)},
  vector_auto{pos(4.0, -2.8), pos_polar(0.0, pi)},
  // едем
  vector_auto{pos(4.0, -2.8), pos_polar(0.4, -pi/2)},
  // 9
  vector_auto{pos(3.8, -5.0), pos_polar(0.4, pi)},
  vector_auto{pos(-0.8, -5.0), pos_polar(0.4, pi)},
  // 10
  vector_auto{pos(-1.1, -2.8), pos_polar(0.2, pi/2)},
  vector_auto{pos(-0.9, -2.2), pos_polar(0.4, pi/2)},
  // 11
  vector_auto{pos(1.3, -1.1), pos_polar(0.4, 0.0)},
  vector_auto{pos(2.3, -1.0), pos_polar(0.4, 0.0)},
  // 12
  vector_auto{pos(5.0, -2.8), pos_polar(0.4, -pi/2)},
  vector_auto{pos(5.0, -3.1), pos_polar(0.4, -pi/2)},
  // 13
  vector_auto{pos(1.3, -6.0), pos_polar(0.4, pi)},
  // 14
  vector_auto{pos(-2.01, -2.8), pos_polar(0.2, pi/2)},
  // 15
  vector_auto{pos(1.3, -2.1), pos_polar(0.4, 0.0)},
  vector_auto{pos(2.3, -2.0), pos_polar(0.4, 0.0)},
  // 16
  vector_auto{pos(6.0, 0.8), pos_polar(0.4, -pi/2)},
  vector_auto{pos(6.0, 0.1), pos_polar(0.4, -pi/2)},
};


class TrackingProxy: public Tracking {
public:
  TrackingProxy(const track_points &track, const metric_points &metric)
    : Tracking(track, metric) {}

  std::vector<track_geom::pos_polar> &GetSegments() { return segments_; }
  std::vector<size_t> GetCandidates() { return getCandidates(); }
  size_t GetNearestCandidate(const std::vector<size_t> &candidates) {
    return getNearestCandidate(candidates);
  }
  void EraseConflicted(std::vector<size_t> *candidates) { eraseConflicted(candidates); }
};


class TrackingTest: public ::testing::Test {
protected:

protected:
  void SetUp() {
    trackp.reset(new TrackingProxy(ulit, mp));
  }

protected:
  std::unique_ptr<TrackingProxy> trackp;
};

/**
 * \brief Тест инициализации сегментов трассы по точкам
 * */
TEST_F(TrackingTest, SegmentsInit) {
  //
  std::vector<pos_polar> a = {
    pos_polar(1.0, pi), pos_polar(1.0, pi / 2.0), pos_polar(2.0, 0.0), pos_polar(2.0, 1.5 * pi),
    pos_polar(3.0, pi), pos_polar(3.0, pi / 2.0), pos_polar(4.0, 0.0), pos_polar(4.0, 1.5 * pi),
    pos_polar(5.0, pi), pos_polar(5.0, pi / 2.0), pos_polar(6.0, 0.0), pos_polar(6.0, 1.5 * pi),
    pos_polar(7.0, pi), pos_polar(7.0, pi / 2.0), pos_polar(8.0, 0.0), pos_polar(3.0, 1.5 * pi),
  };
  auto e = trackp->GetSegments();
  ASSERT_EQ(a.size(), e.size());
  for (size_t i = 0; i < e.size(); ++i) {
    EXPECT_TRUE(is_equal(e[i].r, a[i].r));
    EXPECT_TRUE(is_equal(e[i].f, a[i].f));
  }

  // another one
  Tracking::track_points t {
    pos(0.0, 0.0), pos(1.0, 0.0), pos(1.0, 1.0), pos(0.0, 1.0), pos(1.0, 0.0),
    pos(2.0, -1.0), pos(-3.0, -1.0)
  };
  Tracking::metric_points mp {
    vector_auto{pos(0.0, 0.0), pos_polar(0.0, 0.0)}
  };
  TrackingProxy tp(t, mp);
  auto &d = tp.GetSegments();
  std::vector<pos_polar> p = {
    pos_polar(1.0, 0.0), pos_polar(1.0, pi / 2.0), pos_polar(1.0, pi),
    pos_polar(std::sqrt(2.0), -pi / 4.0), pos_polar(std::sqrt(2.0), -pi / 4.0),
    pos_polar(5.0, pi)
  };
  ASSERT_EQ(d.size(), p.size());
  for (size_t i = 0; i < p.size(); ++i) {
    EXPECT_TRUE(is_equal(p[i].r, d[i].r));
    EXPECT_TRUE(is_equal(p[i].f, d[i].f));
  }
}

TEST_F(TrackingTest, GetCandidates) {
  std::vector<size_t> t = {0, 4, 8, 12};
  Tracking::metric_points mp1(mp.begin() + 5, mp.end());
  TrackingProxy tp(ulit, mp1);
  auto p = tp.GetCandidates();
  ASSERT_EQ(p.size(), t.size());
  for (size_t i = 0; i < p.size(); ++i)
    EXPECT_EQ(t[i], p[i]);

  auto o = trackp->GetCandidates();
  ASSERT_EQ(o.size(), 15);
  for (size_t i = 0; i < o.size(); ++i)
    EXPECT_EQ(o[i], i);
}

TEST_F(TrackingTest, GetNearestCandidates) {
  // m[0] = (1.5, -3.0)
  EXPECT_EQ(0, trackp->GetNearestCandidate(std::vector<size_t> {0, 1, 3}));
  EXPECT_EQ(1, trackp->GetNearestCandidate(std::vector<size_t> {1, 2, 3, 4}));
  EXPECT_EQ(9, trackp->GetNearestCandidate(std::vector<size_t> {7, 9, 10, 11, 12}));
}

/**
 * \brief Тест функции eraseConflicted
 * */
TEST_F(TrackingTest, EraseConflicted) {
  std::vector<size_t> candidates;
  trackp->EraseConflicted(&candidates);
  EXPECT_EQ(candidates.size(), 0);
  // правильное предположение
  candidates = {0, 4, 8, 12};
  trackp->EraseConflicted(&candidates);
  ASSERT_EQ(candidates.size(), 1);
  EXPECT_EQ(candidates[0], 0);
  // неправильное предположение
  candidates = {4, 8, 12};
  trackp->EraseConflicted(&candidates);
  EXPECT_EQ(candidates.size(), 3);
  // правильное предположение
  candidates = {0, 1, 2, 3, 4, 8, 12};
  trackp->EraseConflicted(&candidates);
  ASSERT_EQ(candidates.size(), 1);
  EXPECT_EQ(candidates[0], 0);
  // правильное предположение
  candidates = {1, 2, 3, 4, 8, 12};
  trackp->EraseConflicted(&candidates);
  ASSERT_EQ(candidates.size(), 6);
}
