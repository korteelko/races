cmake_minimum_required(VERSION 3.9)

set(MAIN_PROJECT tracking)
project(${MAIN_PROJECT}-tests)

set(MAIN_PROJECT_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/..")
add_subdirectory("${MAIN_PROJECT_ROOT}/lib/gtest" "lib/gtest")
include_directories(${MAIN_PROJECT_ROOT}/source)

set(TEST_NAME test_track_geom)
add_executable(${TEST_NAME}
    ${MAIN_PROJECT_ROOT}/source/tracking.cpp
    test_tracking.cpp
    test_track_geom.cpp)

target_link_libraries(${TEST_NAME} gtest gtest_main)

