#include "tracking.h"

#include "gtest/gtest.h"


static float pi = 3.1415926535;

using namespace track_geom;

TEST(Distance, Calculate) {
  pos p1(0.0, 0.0);
  pos p2(0.0, 0.0);
  EXPECT_NEAR(distance(p1,p2), 0.0, 0.0001);
  p1.x = 1.0;
  EXPECT_NEAR(distance(p1,p2), 1.0, 0.0001);
  p1.y = 1.0;
  EXPECT_NEAR(distance(p1,p2), 1.41421, 0.0001);
  p2.x=p2.y=1.0;
  EXPECT_NEAR(distance(p1,p2), 0.0, 0.0001);
  p2.x=p2.y=3.0;
  EXPECT_NEAR(distance(p1,p2), 2.828427, 0.0001);
}

TEST(Angle3, Calculate) {
  EXPECT_NEAR(angle3(pos(1.0, 0.0), pos(0.0, 0.0), pos(1.0, 0.0)), 0.0, 0.0001);
  EXPECT_NEAR(angle3(pos(1.0, 0.0), pos(0.0, 0.0), pos(0.0, 1.0)), pi / 2.0, 0.0001);
  EXPECT_NEAR(angle3(pos(1.0, 0.0), pos(0.0, 0.0), pos(1.0, 1.0)), pi / 4.0, 0.0001);
}

TEST(Lims, Calculate) {
  pos A(0.0, 0.0);
  pos B(1.0, 0.0);
  EXPECT_TRUE(is_in_lims(pos(0.0, 0.0), A, B));
  EXPECT_TRUE(is_in_lims(pos(0.0, 1.0), A, B));
  EXPECT_TRUE(is_in_lims(pos(0.5, 0.0), A, B));
  EXPECT_TRUE(is_in_lims(pos(0.5, 10000.0), A, B));
  EXPECT_TRUE(is_in_lims(pos(0.5, -10000.0), A, B));

  EXPECT_FALSE(is_in_lims(pos(1.001, -1.0), A, B));
  EXPECT_FALSE(is_in_lims(pos(-0.001, 0.0), A, B));
  EXPECT_FALSE(is_in_lims(pos(-100.1, 0.0), A, B));
}

TEST(Normal, Calculate) {
  pos A(0.0, 0.0);
  pos B(1.0, 0.0);
  pos p1(0.0, 1.0);
  pos p2(0.0, -1.0);
  pos p3(-1.0, -1.0);
  EXPECT_NEAR(normal_len(A,A,B), 0.0, 0.0001);
  EXPECT_NEAR(normal_len(p1,A,B), 1.0, 0.0001);
  EXPECT_NEAR(normal_len(p2,A,B), 1.0, 0.0001);
  EXPECT_NEAR(normal_len(p3,A,B), 1.0, 0.0001);
  B = pos(0.0, 1.0);
  pos p4(1.0, 0.0);
  pos p5(-1.0, 0.0);
  pos p6(-8.0, 0.0);
  EXPECT_NEAR(normal_len(p4,A,B), 1.0, 0.0001);
  EXPECT_NEAR(normal_len(p5,A,B), 1.0, 0.0001);
  EXPECT_NEAR(normal_len(p6,A,B), 8.0, 0.0001);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
