#include "tracking.h"

#include "examples/examples.h"

#include <iostream>

#include <assert.h>


int main(int argc, char *argv[]) {
  run_example1();
  std::cout << " second example " << std::endl;
  run_example2();
  return 0;
}
