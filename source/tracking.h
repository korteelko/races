#ifndef TRACKING_H
#define TRACKING_H

#include <iostream>
#include <vector>


typedef float distance_t;
/**
 * \brief Проверка равенства чисел с пл. точкой
 * */
bool is_equal(distance_t a, distance_t b, distance_t accur = 0.00001);

/* спрячем функции геометрии */
namespace track_geom {
/*   Наименования
 * x - абсцисса
 * y - ордината
 * r - радиус
 * f - угол[0,2pi) */
/**
 * \brief Позиция в декартовых координах
 * */
struct pos {
public:
  pos() = default;
  pos(distance_t _x, distance_t _y);

  bool operator==(const pos &o) const;

public:
  distance_t x, y;
};
std::ostream &operator<<(std::ostream &str, const pos &p);
/**
 * \brief Позиция в полярных координах
 * */
struct pos_polar {
public:
  pos_polar() = default;
  pos_polar(distance_t _r, float _f);

public:
  distance_t r;
  float f;
};

/**
 * \brief Расстояние между p1 и p2
 * */
distance_t distance(pos p1, pos p2);
/**
 * \brief Длина нормали от точки до прямой заданной точками p1,p2
 * \param point Исследуемая точка
 * \param p1 Точка на прямой
 * \param p2 Точка на прямой
 * */
distance_t normal_len(pos point, pos p1, pos p2);
/**
 * \brief Расчитать угол p1p2p3
 * \return [0, 2pi)
 * */
float angle3(pos p1, pos p2, pos p3);

/**
 * \brief Проверить, что точка в границах(проецируется на отрезок lr)
 * \param point Исследуемая точка
 * \param l Левая(условно) граница отрезка
 * \param r Правая граница отрезка
 * */
bool is_in_lims(pos point, pos l, pos r);
/**
 * \brief Представление вектора в виде данных с авто
 * */
struct vector_auto {
  /* начало вектора */
  pos p;
  /* вектор скорости */
  pos_polar v;
};
}  // namespace track_geom


/**
 * \brief Класс расчёта(сопоставления) и хранения проекции
 * */
class Tracking {
public:
  /* для входных данных */
  typedef std::vector<track_geom::pos> track_points;
  typedef std::vector<track_geom::vector_auto> metric_points;
  /* для выходных данных
   * todo: тут что с форматами? спросить что ожидается на выходе */
  /**
   * \brief Точка сопоставляемая точке метрики датчика. Соответственно,
   *   в полях не определенная ссылка на оригинальную точку метрики,
   *   т.к. сопоставляются по индексу.
   * */
  struct projection_pos {
    /**
     * \brief Индекс отрезка в массиве координат представления трассы
     * */
    size_t track_section_id;
    /**
     * \brief Проекция неточной позиции от датчика на отрезок трассы
     * \note Вот это наверное не особо нужно, без контекста не очень понятно
     * */
    track_geom::pos projected_pos;
  };
  typedef std::vector<projection_pos> projection_points;

public:
  /**
   * \brief Обычный конструктор
   * \param psi_eps Погрешность измерения угла вектора скорости
   * \param track Координаты узлов ломанной представления трассы
   * \param metric Данные с авто
   * */
  Tracking(const track_points &track, const metric_points &metric,
      float psi_eps = 0.001);
  /**
   * \brief Получить ссылку на результат
   * */
  const projection_points &GetProjection() const;

  virtual ~Tracking() = default;

protected:
  /**
   * \brief Рассчитать проекцию телеметрии
   * */
  void calculate();
  /**
   * \brief Инициализировать контейнер segments_
   * */
  void initSegments();
  /**
   * \brief Найти индекс сектора, соответствующий точке metric[0]
   * */
  size_t startSector();
  /**
   * \brief Собрать вектор допустимых начальных секторов
   * */
  std::vector<size_t> getCandidates();
  /**
   * \brief Расчитать наиближайший(по геометрии) сектор из списка кандидатов
   * */
  size_t getNearestCandidate(const std::vector<size_t> &candidates);
  /**
   * \brief Среди предположительных стартовых сегментов оставить
   *   только подходящие
   * \param candidates указатель на вектор начальных сегментов
   * */
  void eraseConflicted(std::vector<size_t> *candidates);

protected:
  /**
   * \brief Ссылка на контейнер данных трассы
   * */
  const track_points &track_;
  /**
   * \brief Ссылка на контейнер данных телеметрии
   * */
  const metric_points &metric_;
  /**
   * \brief Контейнер сегментов трассы в векторном представлении(len, psi)
   * \note Естественно segments_.size() = track_.size() - 1
   * */
  std::vector<track_geom::pos_polar> segments_;
  /**
   * \brief Контейнер результатов
   * \note metric.size() == projected.size()
   * */
  projection_points projected_;
  /**
   * \brief Погрешность измерения угла вектора скорости
   * */
  const float psi_eps_;
};

#endif  // !TRACKING_H
