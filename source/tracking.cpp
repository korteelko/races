#include "tracking.h"

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <limits>
#include <set>
#include <tuple>

#include <assert.h>


static const distance_t pi = 3.1415926;
static const distance_t pi2 = 6.283186;
// точность по distance_t(машинный ноль ПК или датчиков)
static const distance_t eps = 0.00001;

bool is_equal(distance_t a, distance_t b, distance_t accur) {
  return (std::abs(a-b) < accur);
}

namespace track_geom {
pos::pos(distance_t _x, distance_t _y)
  : x(_x), y(_y) {}

bool pos::operator==(const pos &o) const {
  return is_equal(x, o.x) && is_equal(y, o.y);
}

std::ostream &operator<<(std::ostream &str, const pos &p) {
  return str << "(" << p.x << ", " << p.y << ")";
}

pos_polar::pos_polar(distance_t _r, distance_t _f)
  : r(_r), f(_f) {
  if (is_equal(r, 0.0)) {
    f = 0.0;
  } else if (f > pi2) {
    int i = f / pi2;
    f = f - distance_t(i) * pi2;
  } else if (f < 0.0) {
    int i = -f / pi2;
    f = f + distance_t(i + 1) * pi2;
  }
}

distance_t distance(pos p1, pos p2) {
  return std::sqrt(std::pow(p2.x - p1.x, 2.0) + std::pow(p2.y - p1.y, 2.0));
}

distance_t normal_len(pos point, pos p1, pos p2) {
  return (p1 == p2) ? std::numeric_limits<distance_t>::max() :
      std::abs((p2.y - p1.y)*point.x - (p2.x - p1.x)*point.y + p2.x*p1.y - p2.y*p1.x) /
      std::sqrt(std::pow(p2.y - p1.y, 2.0) + std::pow(p2.x - p1.x, 2.0));
}

float angle3(pos p1, pos p2, pos p3) {
  auto r = std::atan2((p3.y - p2.y)*(p1.x - p2.x) - (p1.y - p2.y)*(p3.x - p2.x),
      ((p3.x - p2.x)*(p1.x - p2.x) + (p3.y - p2.y)*(p1.y - p2.y)));
  return (r < 0.0) ? pi2 + r : r;
}

/* если ABC < 90 и BAC < 90, то точка падает на AB */
bool is_in_lims(track_geom::pos point, pos l, pos r) {
  float left_angle,
        right_angle = track_geom::angle3(l, r, point);
  if (right_angle < (0.5 * pi + eps) || right_angle > (1.5 * pi - eps)) {
    left_angle = track_geom::angle3(r, l, point);
    if (left_angle < (0.5 * pi + eps) || left_angle > (1.5 * pi - eps))
      return true;
  }
  return false;
}
}  // namespace track_geom

/* Tracking */
Tracking::Tracking(const Tracking::track_points &track,
    const Tracking::metric_points &metric, float psi_eps)
  : track_(track), metric_(metric), psi_eps_(psi_eps) {
  calculate();
}

const Tracking::projection_points &Tracking::GetProjection() const {
  return projected_;
}

void Tracking::calculate() {
  if ((track_.size() < 2) || metric_.empty())
    return;
  initSegments();
  projected_ = projection_points(metric_.size());
  // индекс в metric
  size_t current_metric = 0;

  // tck
  size_t first_sector = startSector();

  if (first_sector < track_.size()) {
    // начало сегмента
    const track_geom::pos *segment_start = &track_[first_sector];
    for (auto t_point = track_.cbegin() + first_sector + 1; t_point != track_.cend() &&
        current_metric < metric_.size(); ++t_point) {
      // длина и угол наклона сегмента
      auto segment_data = segments_[std::distance(track_.cbegin() + 1, t_point)];
      // переменные определения проекции точки метрики на ломанную
      distance_t dx = t_point->x - segment_start->x,
          dy = t_point->y - segment_start->y;

      while (current_metric < metric_.size()) {
        // если наклон вектора скорости изменился относительно наклона сегмента,
        //   то авто уже на другом сегменте
        //   исключение составляют углы ломанной описсывающей трассу равные 0 гр.
        // тогда, т.к. вдоль трассы смещения невозможны(по условию задачи),
        //   проекция точки выскочит в др. сегмент
        // не рассматривается случай разворота,
        //   только случай остановки(тогда |v| < eps и psi не определено)
        if (metric_[current_metric].v.r > eps) {
          bool angle_diff =
              std::abs(segment_data.f - metric_[current_metric].v.f) > psi_eps_;
          if (angle_diff)
            break;
        }
        // либо тот же сегмент, либо между сегментами угол в 0 гр,
        //   либо скорость = 0
        auto m_point_angle = track_geom::angle3(
            metric_[current_metric].p, *segment_start, *t_point);
        auto m_projection = track_geom::distance(*segment_start,
            metric_[current_metric].p) * std::cos(m_point_angle);
        // если проекция следующей точки метрики вне данного сегмента
        //   переходим к следующему
        if (m_projection < segment_data.r + eps) {
          // записывем проекцию в вектор результатов
          projected_[current_metric].track_section_id =
              std::distance(track_.cbegin(), t_point);
          float part = m_projection / segment_data.r;
          // из-за неточности позиционирования на пересечении сегментов
          //   метрика может попасть в "слепую" зону
          //   прочие кака датчика тоже здесь должны свалиться
          if (part < 0.0)
            part = 0.0;
          projected_[current_metric].projected_pos = track_geom::pos(
              segment_start->x + part * dx, segment_start->y + part * dy);
        } else {
          // пересчитать эту точку на след. сегменте
          break;
        }
        ++current_metric;
      }
      segment_start = t_point.base();
    }
  }
  // дописать точки метрики за трэком в конец
  while (current_metric < projected_.size()) {
    projected_[current_metric].track_section_id = track_.size() - 1;
    projected_[current_metric++].projected_pos = track_.back();
  }
}

void Tracking::initSegments() {
  if (track_.size() > 1)
    segments_ = std::vector<track_geom::pos_polar>(track_.size() - 1);

  size_t i = 0;
  auto ss = track_.cbegin();
  for (auto se = track_.cbegin() + 1;
      se != track_.cend(); ++se) {
    // длина сегмента
    segments_[i].r = distance(*ss, *se);
    // угол наклона сегмента исследуемого
    segments_[i++].f = track_geom::angle3(
        track_geom::pos(ss->x + 1.0, ss->y),*ss, *se);
    ss = se;
  }
}

/*
 * боимся вот такой конфигурации:
 * y             ___
 *  ^        __ |   |
 *  |  __ __ || | _D|
 *  |  || || || ||  |
 *  |  || || || ||  |
 *  |  A\_B\/ C/ |__|
 *  |------------------ > x
 *
 * A --> D
 * стартовать можно и с A и с B, а вот закончить
 * приём данных в C, а не в D
 **/
size_t Tracking::startSector() {
  // стартуем
  size_t ind = 0;
  // контейнер подходящих индексов сегментов
  std::vector<size_t> candidates = getCandidates();
  if (candidates.size() == 1) {
    ind = candidates[0];
  } else if (candidates.size() > 1) {
    // из списка подходящих для начала телеметрии сегментов
    //   исключить несогласованные, недопустимые
    eraseConflicted(&candidates);
    // индекс наиближвйшего сектора
    ind = getNearestCandidate(candidates);
  }
  return ind;
}

std::vector<size_t> Tracking::getCandidates() {
  /* todo: вроде можно скостить пару вычислений на вытаскивании угла из
   *   функции in_in_lims */
  auto m = metric_[0];
  std::vector<size_t> candidates;
  if (m.v.r) {
    // если в движении - ищем по углам, а потом ближайщую(если есть параллельные сегменты),
    //   хотя правильнее не ближайщую к прямой искать(позиция не точна) и её
    //   проекцию на сегмент, лучше пробежать по телементрии записать
    //   все смены направлений. Похоже на задачу максимальной подстроки
    for (auto x = segments_.cbegin(); x != segments_.cend(); ++x) {
      if (is_equal(x->f, m.v.f, psi_eps_)) {
        // сразу проверим, что точка в границах отрезка
        auto i = std::distance(segments_.cbegin(), x);
        // индекыс из вектора segments_
        if (track_geom::is_in_lims(m.p, track_[i], track_[i + 1]))
          candidates.push_back(i);
      }
    }
  } else {
    // авто стоит, не едет,
    //   все сектора перебираем, ищем те на которые проецируется точка
    auto ss = track_.cbegin();
    for (auto se = track_.cbegin() + 1; se != track_.cend(); ++se) {
      if (track_geom::is_in_lims(m.p, *ss, *se))
        candidates.push_back(std::distance(track_.cbegin(), ss));
      ss = se;
    }
  }
  return candidates;
}

size_t Tracking::getNearestCandidate(const std::vector<size_t> &candidates) {
  auto m = metric_[0];
  // todo: ups, add exception
  size_t min_i = candidates[0];
  distance_t dmin = track_geom::normal_len(m.p, track_[min_i], track_[min_i + 1]);
  for (auto i = candidates.cbegin() + 1; i != candidates.cend(); ++i) {
    auto d = track_geom::normal_len(m.p, track_[*i], track_[*i + 1]);
    if (d < dmin) {
      dmin = d;
      min_i = *i;
    }
  }
  return min_i;
}

void Tracking::eraseConflicted(std::vector<size_t> *candidates) {
  std::vector<float> m_angles;
  m_angles.reserve(metric_.size());
  // если на всех точках телеметрии авто стоит
  auto angle_it = std::find_if(metric_.cbegin(), metric_.cend(),
      [](const track_geom::vector_auto &m){ return !is_equal(m.v.r, 0.0); });
  if (angle_it != metric_.cend()) {
    // переменная для отслеживания углов в 180 гр
    float angle = angle_it->v.f;
    // составим карту поворотов
    // m_angles.size() == 0 если v.r == 0.0 для всех
    m_angles.push_back(angle);
    for (auto x = angle_it + 1; x != metric_.cend(); ++x)
      if (!is_equal(angle, x->v.f) && (x->v.r > eps))
        m_angles.push_back(angle = x->v.f);

  }
  struct Node {
    /** id сегмента */
    size_t segment_id;
    /** номер поворота в последовательности метрик */
    ssize_t angle_num;

    bool operator< (const Node &r) const {
      return std::tie(segment_id, angle_num) <
          std::tie(r.segment_id, r.angle_num);
    }
  };
  std::set<Node> source;
  std::vector<size_t> tails;
  for (size_t start: *candidates) {
    // нада сопоставить все повороты авто с конфигурацией трассы
    auto angle_it = m_angles.begin();
    std::set<Node> candidate_source;
    bool valid_path = false;
    for (size_t d = start; d < segments_.size(); ++d) {
      if (is_equal(segments_[d].f, *angle_it)) {
        Node n {d, std::distance(m_angles.begin(), angle_it)};
        if (source.find(n) != source.end()) {
          // если такой элемент уже есть, след. элементы совпадают.
          //   Обновим на более вероятный вариант(этот)
          tails.back() = start;
          break;
        }
        candidate_source.emplace(n);
        ++angle_it;
        if (angle_it == m_angles.end()) {
          // все нашлись, пройдя полный путь
          valid_path = true;
          tails.push_back(start);
          break;
        }
      }
    }
    if (valid_path)
      // если путь валиден, будем отслеживаем узлы этого пути
      source.merge(candidate_source);
  }
  if (tails.size()) {
    *candidates = std::vector<size_t>(tails.size());
    std::transform(tails.begin(), tails.end(), candidates->begin(),
        [](auto p) {return p;});
  }
}
